import { forEach, forOwn } from 'lodash';

const suits = Object.freeze({
  hearts: 'hearts',
  spades: 'spades',
  club: 'clubs',
  diamonds: 'diamonds',
});

const faceValues = Object.freeze({
  ace: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
  ten: 10,
  jack: 11,
  queen: 12,
  king: 13,
});

const getOrderedDeckOfCards = () => {
  const deck = [];

  forEach(suits, (suit) => {
    forOwn(faceValues, (value, name) => {
      deck.push({
        name,
        suit,
        value,
      });
    });
  });

  return deck;
};

module.exports = {
  suits,
  faceValues,
  getOrderedDeckOfCards,
};
