import { cloneDeep, pullAt, times } from 'lodash';
import { RESET_DECK, SHUFFLE_DECK, DRAW_CARD } from '../actions/deck';
import { getOrderedDeckOfCards } from '../../config/cards';
import { shuffleArray } from '../../util/util';

const getDefaultDeckState = () => ({
  deck: {
    unturned: getOrderedDeckOfCards(),
    discarded: [],
    shuffled: 0,
  },
});

export default function deck(state = getDefaultDeckState(), action) {
  const newState = cloneDeep(state);

  switch (action.type) {
    case RESET_DECK:
      return getDefaultDeckState();
    case SHUFFLE_DECK:
      times(action.turns, shuffleArray(newState.deck.unturned));
      newState.deck.shuffled += action.turns;
      return newState;
    case DRAW_CARD:
      if (newState.deck.unturned.length > 0) {
        newState.deck.discarded.push(pullAt(newState.deck.unturned, 0)[0]);
      }
      return newState;
    default:
      return newState;
  }
}
