export const RESET_DECK = 'RESET_DECK';
export const SHUFFLE_DECK = 'SHUFFLE_DECK';
export const DRAW_CARD = 'DRAW_CARD';

export function resetDeck() {
  return { type: RESET_DECK };
}

export function shuffleDeck(turns) {
  return {
    type: SHUFFLE_DECK,
    turns,
  };
}

export function drawCard() {
  return {
    type: DRAW_CARD,
  };
}
