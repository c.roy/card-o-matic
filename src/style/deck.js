const deckStyle = {
  textAlign: 'center',
  paddingTop: '10%',
  paddingBottom: '10%',
};

const titleStyle = {
  fontSize: '35px',
};

const shuffleCounterStyle = {
  marginTop: '5px',
};

const pileStyle = {
  display: 'inline-block',
  width: '15%',
  height: 'auto',
};

const cardStyle = {
  width: '100%',
  height: 'auto',
  paddingLeft: '1%',
  paddingRight: '1%',
  paddingBottom: '1%',
};

const menuStyle = {
  paddingTop: '1%',
};

module.exports = {
  deckStyle,
  titleStyle,
  shuffleCounterStyle,
  pileStyle,
  cardStyle,
  menuStyle,
};
