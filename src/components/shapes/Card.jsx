import PropTypes from 'prop-types';

export default PropTypes.shape({
  name: PropTypes.string,
  suit: PropTypes.string,
  value: PropTypes.number,
});
