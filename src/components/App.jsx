import { createStore } from 'redux';
import { Provider } from 'react-redux';

import deckReducer from '../redux/reducers/deck';

import DeckContainer from './deck/DeckContainer';

const store = createStore(
  deckReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const App = () => (
  <Provider store={store}>
    <DeckContainer />
  </Provider>
);

export default App;
