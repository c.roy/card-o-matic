import { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { menuStyle } from '../../style/deck';

class DeckMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleDrawClick = this.handleDrawClick.bind(this);
    this.handleShuffleClick = this.handleShuffleClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
  }

  handleDrawClick() {
    this.props.drawCard();
  }

  handleShuffleClick() {
    this.props.shuffleDeck(1);
  }

  handleResetClick() {
    this.props.resetDeck();
  }

  render() {
    return (
      <div style={menuStyle}>
        <Button content="Draw Card" onClick={this.handleDrawClick} primary />
        <Button content="Shuffle Deck" onClick={this.handleShuffleClick} secondary />
        <Button content="Reset Deck" onClick={this.handleResetClick} secondary />
      </div>
    );
  }
}

DeckMenu.propTypes = {
  drawCard: PropTypes.func.isRequired,
  resetDeck: PropTypes.func.isRequired,
  shuffleDeck: PropTypes.func.isRequired,
};

export default DeckMenu;
