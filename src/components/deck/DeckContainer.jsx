import { connect } from 'react-redux';
import { resetDeck, shuffleDeck, drawCard } from '../../redux/actions/deck';
import DeckSection from './DeckSection';

const mapStateToProps = state => ({
  deck: state.deck,
});

const mapDispatchToProps = dispatch => ({
  resetDeck: () => dispatch(resetDeck()),
  shuffleDeck: turns => dispatch(shuffleDeck(turns)),
  drawCard: () => dispatch(drawCard()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeckSection);
