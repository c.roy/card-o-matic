import { Component } from 'react';
import PropTypes from 'prop-types';
import cardShape from '../shapes/Card';
import UnturnedPile from '../piles/UnturnedPile';
import DiscardPile from '../piles/DiscardPile';
import DeckMenu from './DeckMenu';

import { deckStyle, titleStyle, shuffleCounterStyle } from '../../style/deck';

class DeckSection extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { deck } = this.props;

    return (
      <div style={deckStyle}>
        <h1 style={titleStyle}>Card-O-Matic</h1>
        <UnturnedPile size={deck.unturned.length} />
        <DiscardPile pile={deck.discarded} />
        <DeckMenu
          drawCard={this.props.drawCard}
          resetDeck={this.props.resetDeck}
          shuffleDeck={this.props.shuffleDeck}
        />
        <p style={shuffleCounterStyle} >{`The deck has been shuffled ${deck.shuffled} times.`}</p>
      </div>
    );
  }
}

DeckSection.propTypes = {
  deck: PropTypes.shape({
    unturned: PropTypes.arrayOf(cardShape),
    discarded: PropTypes.arrayOf(cardShape),
  }).isRequired,
  drawCard: PropTypes.func.isRequired,
  resetDeck: PropTypes.func.isRequired,
  shuffleDeck: PropTypes.func.isRequired,
};

export default DeckSection;
