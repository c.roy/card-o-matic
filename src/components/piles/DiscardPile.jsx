import PropTypes from 'prop-types';

import { cardStyle, pileStyle } from '../../style/deck';
import cardSVG from '../../assets/cardsSVG';
import cardShape from '../shapes/Card';

const getNumberOfCardsMessage = size => (
  <p>{`Cards in pile: ${size}`}</p>
);

const DiscardPile = (props) => {
  const size = props.pile.length;
  const card = size > 0 ? props.pile[size - 1] : null;

  if (card) {
    return (
      <div style={pileStyle}>
        <img style={cardStyle} src={cardSVG[`${card.suit}`][`${card.value}`]} alt={`${card.name} of ${card.suit}`} />
        {getNumberOfCardsMessage(size)}
      </div>
    );
  }

  return <div style={pileStyle}>{getNumberOfCardsMessage(size)}</div>;
};

DiscardPile.propTypes = {
  pile: PropTypes.arrayOf(PropTypes.shape(cardShape)).isRequired,
};

export default DiscardPile;
