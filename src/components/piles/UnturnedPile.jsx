import PropTypes from 'prop-types';

import { cardStyle, pileStyle } from '../../style/deck';
import cardsSVG from '../../assets/cardsSVG';

const getNumberOfCardsMessage = size => (
  <p>{`Cards in pile: ${size}`}</p>
);

const UnturnedPile = (props) => {
  const { size } = props;

  if (size) {
    return (
      <div style={pileStyle}>
        <img style={cardStyle} src={cardsSVG.back} alt="Back of playing card" />
        {getNumberOfCardsMessage(size)}
      </div>
    );
  }

  return <div style={pileStyle}>{getNumberOfCardsMessage(size)}</div>;
};

UnturnedPile.propTypes = {
  size: PropTypes.number.isRequired,
};

export default UnturnedPile;
