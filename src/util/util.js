import { random } from 'lodash';

const shuffleArray = (arr) => {
  const shuffledArray = arr;
  let i = shuffledArray.length;

  while (i) {
    const target = random(0, i--);

    const temp = shuffledArray[i];
    shuffledArray[i] = shuffledArray[target];
    shuffledArray[target] = temp;
  }

  return shuffledArray;
};

module.exports = {
  shuffleArray,
};
