### Instructions
To start the application:

```
npm install
npm run start
```

The application will then be available on your `localhost`, at port `3000`.